# coding=utf-8
from flask import Flask
from flask_restful import Resource, Api, reqparse
from flask.views import MethodView
from PhotoCombine import generatePhoto
import requests, json
from flask import request
from datetime import date, timedelta
import urllib

app = Flask(__name__)
api = Api(app)

key = 'key_test_ehdSKw37ssan0GXJbvMAUaliByeb0rqW'
#key = 'key_live_efjRLv1XzCanZJZKiqMgyhljrAoh7DxL'

@app.route("/")
def hello():
    return "Hello World!"

class HotelsApi(Resource):

    def __init__(self):
        super(Resource, self).__init__()
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('city_name', type=str, required=False)


    def get(self):
        # args = self.parser.parse_args()
        params = {
                'query': request.args['city_name'],
                'object_type': 'geo',
            }
        print 'get city_id'
        r = requests.get('http://hapi.onetwotrip.com/api/suggestRequest', params=params)

        content = json.loads(r.content)
        city_id = content['result'][0]['city_id']
        suggestRequestObj = content['result'][0]

        today = date.today()
        date_pattern = "%Y-%m-%d"
        date_in = today.strftime(date_pattern)
        date_out = (today + timedelta(days=1)).strftime(date_pattern)

        suggestRequestObj['date_in'] = date_in
        suggestRequestObj['date_out'] = date_out

        params = {
            'date_start': date_in,
            'date_end': date_out,
            'object_id': city_id,
            'object_type': 'geo',
        }

        print 'get images'

        r = requests.get('http://hapi.onetwotrip.com/api/searchRequest', params=params)

        content = json.loads(r.content)
        hotels = content['result']['hotels']
        urls = [hotels[each]['img'] for each in range(len(hotels))][0:3]

        # base64_temp = generatePhoto(urls)

        return {
            'data': suggestRequestObj,
            'image': 'data:image/jpeg;base64,{}'.format(generatePhoto(urls)),
            'href': 'https://bnc.lt/a/{}/?json='.format(key),
        }

api.add_resource(HotelsApi, '/api/v1', endpoint='api')
# api.add_resource(HotelsApi, '/api/v1/get_round_fly', endpoint='api')

if __name__ == '__main__':
    app.run(host='0.0.0.0')