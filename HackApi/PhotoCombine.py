from __future__ import print_function
import urllib, cStringIO
import base64
import requests
import json
import os

from PIL import Image, ImageOps


def generatePhoto(urls):
    width = 230
    height = 230

    result = Image.new("RGB", (width * 3, height))
    size = (width, height)
    for index, URL in enumerate(urls):

        file = cStringIO.StringIO(urllib.urlopen(URL).read())
        # print('url: {0}'.format(URL))
        img = Image.open(file)

        thumb = ImageOps.fit(img, size, Image.ANTIALIAS)
        x = index * width
        w, h = thumb.size
        # print('pos {0},{1} size {2},{3}'.format(x, 0, w, h))
        result.paste(thumb, (x, 0, x + w, 0 + h))

#    result.save(os.path.expanduser('image.png'))


    buffer = cStringIO.StringIO()
    result.save(buffer, format="JPEG")


    return base64.b64encode(buffer.getvalue())



if __name__ == '__main__':

    params = {
        'date_start': '2015-11-07',
        'date_end': '2015-11-08',
        'object_id': '5128581',
        'object_type': 'geo',
    }

    r = requests.get('http://hapi.onetwotrip.com/api/searchRequest', params=params)

    content = json.loads(r.content)
    hotels = content['result']['hotels']
    urls = [hotels[each]['img'] for each in range(len(hotels))][0:3]

    base64_temp = generatePhoto(urls)

    print(base64_temp)

