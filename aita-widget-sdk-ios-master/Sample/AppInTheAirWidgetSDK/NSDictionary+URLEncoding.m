//
//  NSDictionary+URLEncoding.m
//  OneTwoTrip
//
//  Created by Anton Bukarev on 11/6/14.
//  Copyright (c) 2014 OneTwoTrip. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NSDictionary+URLEncoding.h"


#pragma mark - NSDictionary+URLEncoding Implementaiton

@implementation NSDictionary (URLEncoding)

#pragma mark - Public Methods

- (NSString *) URLEncodedString
{
  NSMutableArray *parts = [NSMutableArray array];
  
  for (id key in self)
  {
    id value = [self objectForKey: key];
    NSString *keyToEscape = [NSString stringWithFormat:@"%@", key];
    NSString *valueToEscape = [NSString stringWithFormat:@"%@", value];
    NSString *escapedKey = AFPercentEscapedQueryStringPairMemberFromStringWithEncoding (keyToEscape, NSUTF8StringEncoding);
    NSString *escapedValue = AFPercentEscapedQueryStringPairMemberFromStringWithEncoding (valueToEscape, NSUTF8StringEncoding);
    NSString *part = [NSString stringWithFormat: @"%@=%@", escapedKey, escapedValue];
    [parts addObject: part];
  }
  [parts sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
  return [parts componentsJoinedByString: @"&"];
}

//http://stackoverflow.com/questions/17444876/ios-not-encoding-plus-sign-in-x-www-form-urlencoded-post-request
//strings to escape and to leave are copy-pasted from AFNetworking
static NSString * AFPercentEscapedQueryStringPairMemberFromStringWithEncoding(NSString *string, NSStringEncoding encoding)
{
  static NSString * const kAFCharactersToBeEscaped = @":/?&=;+!@#$()~',*";
  static NSString * const kAFCharactersToLeaveUnescaped = @"[].";
  
  return (__bridge_transfer  NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, (__bridge CFStringRef)kAFCharactersToLeaveUnescaped, (__bridge CFStringRef)kAFCharactersToBeEscaped, CFStringConvertNSStringEncodingToEncoding(encoding));
}

@end
