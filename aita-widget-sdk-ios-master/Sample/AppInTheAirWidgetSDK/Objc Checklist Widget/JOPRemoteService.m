//
//  JOPRemoteService.m
//  AppInTheAirWidgetSDK
//
//  Created by Pavel Akhrameev on 31.10.15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

#import "JOPRemoteService.h"

#import "NSURLSession+OTTAdditions.h"
#import "NSDictionary+OTTAdditions.h"
#import "NSDictionary+URLEncoding.h"

static NSString *const kHTTPMethodGET = @"GET";
static NSString *const kJOPDomain = @"JOP";
const NSUInteger kInvalidRequestId = NSUIntegerMax;
const NSInteger kInternalServerErrorCode = 0;
const NSInteger kNoInternetConnectionErrorCode = 1;
const NSInteger kCanceledErrorCode = 2;
const NSInteger kUnknownErrorCode = 3;

#define REPORT_ERROR(e) if (error != nil) *error = e;

@interface JOPRemoteService () <NSURLSessionDelegate>

@property (nonatomic, strong) NSURLSession *URLSession;
@property (nonatomic, strong) NSMutableDictionary *requestIdToCompletionBlock;
@property (nonatomic, strong) NSLock *dictionaryAccessLock;

@end

@implementation JOPRemoteService

@synthesize URLSession = m_URLSession;
@synthesize requestIdToCompletionBlock = m_requestIdToCompletionBlock;
@synthesize dictionaryAccessLock = m_dictionaryAccessLock;

+ (instancetype) sharedService
{
  static JOPRemoteService *_self = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _self = [[self alloc] init];
  });
  return _self;
}

- (instancetype) init
{
  if (self = [super init])
  {
    m_URLSession = [NSURLSession sessionWithDelegate: self];
    m_requestIdToCompletionBlock = [NSMutableDictionary new];
    m_dictionaryAccessLock = [NSLock new];
  }
  return self;
}

- (NSUInteger) generateNextRequestId
{
  static NSLock *lock = nil;
  static NSUInteger currentId = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    lock = [NSLock new];
  });
  
  NSUInteger result = 0;
  [lock lock];
  result = currentId++;
  [lock unlock];
  
  return result;
}

- (NSError *) errorFromRequestError: (NSError *) error
{
  if (error != nil)
  {
    if ([error.domain isEqualToString: NSURLErrorDomain] && (error.code == NSURLErrorCancelled))
    {
      NSLog(@"%@", [self canceledError].localizedDescription);
      return [self canceledError];
    }
    else if ([error.domain isEqualToString: NSURLErrorDomain] &&
             (error.code == NSURLErrorNotConnectedToInternet      ||
              error.code == NSURLErrorNetworkConnectionLost       ||
              error.code == NSURLErrorTimedOut                    ||
              error.code == NSURLErrorInternationalRoamingOff     ||
              error.code == NSURLErrorDataNotAllowed              ||
              error.code == NSURLErrorDataLengthExceedsMaximum    ||
              error.code == NSURLErrorCannotConnectToHost         ||
              error.code == NSURLErrorCannotFindHost))
    {
      NSLog(@"%@", [self noConnectionError].localizedDescription);
      return [self noConnectionError];
    }
    
    return [self unknownError];
  }
  return nil;
}

- (NSError *) noConnectionError
{
  return [NSError errorWithDomain: kJOPDomain
                             code: kNoInternetConnectionErrorCode
                         userInfo: @{NSLocalizedDescriptionKey: NSLocalizedString(@"NO_INTERNET_ERROR_DESCRIPTION", nil)}];
}

- (NSError *) canceledError
{
  return [NSError errorWithDomain: kJOPDomain
                             code: kCanceledErrorCode
                         userInfo: @{NSLocalizedDescriptionKey: NSLocalizedString(@"REQUEST_CANCELED_ERROR_DESCRIPTION", nil)}];
}

- (NSError *) unknownError
{
  return [NSError errorWithDomain: kJOPDomain
                             code: kUnknownErrorCode
                         userInfo: @{NSLocalizedDescriptionKey : NSLocalizedString(@"UNKNOWN_ERROR_DESCRIPTION", nil)}];
}

- (NSError *) internalServerErrorWithUserInfo: (NSDictionary *) userInfo
{
  return [NSError errorWithDomain: kJOPDomain
                             code: kInternalServerErrorCode
                         userInfo: userInfo];
}

- (id) clearRequestWithId: (NSUInteger) requestId
{
  [self.dictionaryAccessLock lock];
  id result = self.requestIdToCompletionBlock[@(requestId)];
  [self.requestIdToCompletionBlock removeObjectForKey: @(requestId)];
  [self.dictionaryAccessLock unlock];
  
  return result;
}

- (NSUInteger) requestRemoteRequestWithCityName: (NSString *) cityName
                                     completion: (RemoteRequestCompletion) completion
{
  NSParameterAssert(completion != nil);
  if (completion == nil)
  {
    return kInvalidRequestId;
  }
  
  NSUInteger requestId = [self generateNextRequestId];
  [self.dictionaryAccessLock lock];
  self.requestIdToCompletionBlock[@(requestId)] = [completion copy];
  [self.dictionaryAccessLock unlock];
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    __weak JOPRemoteService *weakSelf = self;
    
    void(^requestCompletion)(id, id, id) = ^(NSData *data, NSURLResponse *response, NSError *error)
    {
      NSError *internalError = [weakSelf errorFromRequestError: error];
      NSDictionary *locationObject = nil;
      NSData *imageData = nil;
      NSURL *branchURL = nil;
      NSString *requestRemoteID = nil;
      if (internalError != nil)
      {
        error = internalError;
      }
      else
      {
        NSError *e = nil;
        locationObject = [weakSelf locationObjectFromServerResponse: data
                                                        urlResponse: response
                                                    requestRemoteID: &requestRemoteID
                                                          imageData: &imageData
                                                          branchUrl: &branchURL
                                                              error: &e];;
        if (e != nil)
        {
          error = e;
        }
      }
      
      RemoteRequestCompletion completionBlock = [weakSelf clearRequestWithId: requestId];
      if (completionBlock != nil)
      {
        completionBlock(locationObject, imageData, branchURL, requestId, error);
      }
    };
    
    NSDictionary *parameters = @{@"city_name": cityName ? : @"New-York"};
    NSString *paramsStr = [parameters URLEncodedString];
    
    NSString *urlString = [@"http://192.168.1.29:5000/api/v1?" stringByAppendingString: paramsStr];
    
    NSURL *url = [NSURL URLWithString: urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: url];
    [request setHTTPMethod: kHTTPMethodGET];
    
    [[self.URLSession dataTaskWithRequest: request completionHandler: requestCompletion] resume];
  });
  
  return requestId;
}

- (NSDictionary *) locationObjectFromServerResponse: (NSData *) data
                                        urlResponse: (NSURLResponse *) urlResponse
                                    requestRemoteID: (out NSString **) requestRemoteID
                                          imageData: (NSData **) imageData
                                          branchUrl: (NSURL **) branchURL
                                              error: (out NSError **) error
{
  NSDictionary *dic = [NSJSONSerialization JSONObjectWithData: data ? : [NSData data]
                                                      options: 0
                                                        error: nil];
  
  if (![dic isKindOfClass: [NSDictionary class]])
  {
    REPORT_ERROR([self internalServerErrorWithUserInfo: @{ @"reason" : @"invalid response" }])
    return nil;
  }
  
  NSDictionary *locationObject = [dic dictionaryFromPath: @"data"];
  
  if (!locationObject)
  {
    REPORT_ERROR([self internalServerErrorWithUserInfo: nil]);
    return nil;
  }
  
  NSString *imageDataBase64String = [dic stringFromPath: @"image"];
  
  NSURL *url = [NSURL URLWithString: imageDataBase64String];
  NSData *imageDataTmp = [NSData dataWithContentsOfURL: url];
  if (imageDataTmp.length == 0)
  {
    REPORT_ERROR([self internalServerErrorWithUserInfo: nil]);
    return nil;
  }
  if (imageData != nil)
  {
    *imageData = imageDataTmp;
  }

  NSString *branchUrlPrefix = [dic stringFromPath: @"href"];
  if (branchUrlPrefix != nil && branchURL != nil)
  {
    NSString *branchUrlString = [branchUrlPrefix stringByAppendingString: locationObject.URLEncodedString];
    *branchURL = [NSURL URLWithString: branchUrlString];
  }
  
  return locationObject;
}

@end
