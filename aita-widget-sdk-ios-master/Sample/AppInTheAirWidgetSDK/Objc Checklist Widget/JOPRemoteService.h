//
//  JOPRemoteService.h
//  AppInTheAirWidgetSDK
//
//  Created by Pavel Akhrameev on 31.10.15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const NSUInteger kInvalidRequestId;

extern const NSInteger kInternalServerErrorCode;
extern const NSInteger kNoInternetConnectionErrorCode;
extern const NSInteger kCanceledErrorCode;
extern const NSInteger kUnknownErrorCode;

typedef void (^RemoteRequestCompletion)(NSDictionary *, NSData *, NSURL *, NSUInteger, NSError *);

@interface JOPRemoteService : NSObject

+ (instancetype) sharedService;


- (NSUInteger) requestRemoteRequestWithCityName: (NSString *) cityName
                                     completion: (RemoteRequestCompletion) completion;

@end
