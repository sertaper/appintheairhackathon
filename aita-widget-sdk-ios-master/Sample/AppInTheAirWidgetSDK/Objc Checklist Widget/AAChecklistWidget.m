//
//  AAChecklistWidget.m
//  AppInTheAirWidgetSDK
//
//  Created by Sergey Pronin on 10/31/15.
//  Copyright © 2015 App in the Air. All rights reserved.
//

#import "AAChecklistWidget.h"
#import "AppInTheAirWidgetSDK-Swift.h"
#import "JOPRemoteService.h"

@interface AAChecklistWidget()
@property (nonatomic, readwrite) BOOL canBeDisplayed;
@property (nonatomic, copy) UIImage *image;
@property (nonatomic, assign) BOOL customImage;
@property (nonatomic, copy) NSURL *branchURL;
@property (nonatomic, assign) BOOL loading;
@end

@implementation AAChecklistWidget

- (instancetype)initWithDashboard:(Dashboard *)dashboard
                             trip:(Trip *)trip
                           flight:(FlightInfo *)flight {
    self = [super init];
    if (self ) {
        self.dashboard = dashboard;
        self.flight = flight;
        self.trip = trip;
        self.image = [UIImage imageNamed: @"Отели статика ожидание"];
        self.canBeDisplayed = YES;
        [self requestRemoteServerUntilGetData];
    }
    return self;
}

- (void) requestRemoteServerUntilGetData
{
  JOPRemoteService *service = [JOPRemoteService sharedService];
  __weak AAChecklistWidget *weakSelf = self;
  [service requestRemoteRequestWithCityName: self.destinationCityName
                                 completion: ^(NSDictionary *locationObject,
                                               NSData *imageData,
                                               NSURL *branchURL,
                                               NSUInteger remoteRequestId,
                                               NSError * error) {
                                   if (error != nil)
                                   {
                                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                       [weakSelf requestRemoteServerUntilGetData];
                                     });
                                     return;
                                   }
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                     weakSelf.branchURL = branchURL;
                                     weakSelf.image = [[UIImage alloc] initWithData: imageData];
                                     weakSelf.customImage = YES;
                                     [weakSelf.dashboard reloadWidget: weakSelf];
                                   });
                                 }];
}

- (void)configureCell: (UICollectionViewCell *) reusableCell
{
    ChecklistWidgetCell *cell = (ChecklistWidgetCell *)reusableCell;
    cell.labelTitle.text = [self titleText];
    cell.labelTitle.textColor = [UIColor whiteColor];
    cell.labelTitle.backgroundColor = [UIColor colorWithWhite: 0.0 alpha: 0.3];
    cell.backgroundImageView.image = self.image;
  if (!self.customImage)
  {
    [cell.activityIndicator startAnimating];
  }
  else
  {
    [cell.activityIndicator stopAnimating];
  }
}

- (NSString *) destinationCityName
{
  NSString *destinationName = self.trip.name;
  destinationName = [[destinationName componentsSeparatedByString: @"— "] lastObject];
  return destinationName;
}

- (NSString *) titleText
{
  NSString *destinationName = [self destinationCityName];
  if (destinationName == nil)
  {
    return self.name;
  }
  return [self.name stringByAppendingFormat: @" — %@", destinationName];
}

- (BOOL)canBeHidden {
    return YES;
}

+ (NSString *)widgetType {
    return @"sample";
}

- (UIViewController *)viewController {
    //return any if needed
    if (self.branchURL != nil)
    {
      [[UIApplication sharedApplication] openURL: self.branchURL];
    }
    return nil;
}

- (NSString *)name {
    return NSLocalizedString(@"ОТЕЛИ", nil);
}

- (CGFloat)cellHeight {
    return 100;
}

- (NSString *)cellIdentifier {
    return @"ChecklistWidgetCell";
}

@end
