//
//  NSDictionary+URLEncoding.h
//  OneTwoTrip
//
//  Created by Anton Bukarev on 11/6/14.
//  Copyright (c) 2014 OneTwoTrip. All rights reserved.
//


/**
 *  The category that converts the dictionary into URL param list string that looks like A1=B1&A2=B2 where
 *  A's are param names (taken from keys) and B's are param values (taken from values).
 */
@interface NSDictionary (URLEncoding)

/**
 *  The method that converts the dictionary into the string of URL params.
 *
 *  @return The resulting string of URL params.
 */
- (NSString *) URLEncodedString;

@end
