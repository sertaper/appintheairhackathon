
//
//  NSURLSession+OTTAdditions.m
//  OneTwoTrip
//
//  Created by Alexander Skvortsov on 30/07/15.
//  Copyright (c) 2015 OneTwoTrip. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSURLSession+OTTAdditions.h"

@implementation NSURLSession (OTTAdditions)

+ (NSURLSession *) sessionWithDelegate: (id<NSURLSessionDelegate>) delegate
{
  NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
  config.timeoutIntervalForRequest = 60.0;
  config.HTTPCookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
  config.HTTPShouldSetCookies = YES;
  
  NSOperationQueue *delegateQueue = [NSOperationQueue new];
  delegateQueue.maxConcurrentOperationCount = 1;
  
  return [NSURLSession sessionWithConfiguration: config
                                       delegate: delegate
                                  delegateQueue: delegateQueue];
}

@end
