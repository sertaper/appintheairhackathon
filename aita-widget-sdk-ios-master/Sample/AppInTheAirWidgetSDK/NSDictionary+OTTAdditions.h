//
//  NSDictionary+OTTAdditions.h
//  OneTwoTrip
//
//  Created by Alexander Skvortsov on 20/02/15.
//  Copyright (c) 2015 OneTwoTrip. All rights reserved.
//


/**
 *  Extension that simplifies JSON response parsing.
 */
@interface NSDictionary (OTTAdditions)

/**
 *  Gets object of the specified type from the specified path.
 *
 *  @param type Type of the resulting object. Nil skips type check;
 *  @param path String formatted as in example: dict.array[index].fieldName
 *
 *  @return Result with given type or nil if path not found or there's type mismatch.
 */
- (id) objectWithType: (Class) type fromPath: (NSString *) path;

/**
 *  Convenience methods for previous one.
 */
- (NSString *) stringFromPath: (NSString *) path;
- (NSNumber *) numberFromPath: (NSString *) path;
- (NSArray *) arrayFromPath: (NSString *) path;
- (NSDictionary *) dictionaryFromPath: (NSString *) path;
- (id) objectFromPath: (NSString *) path;

@end
