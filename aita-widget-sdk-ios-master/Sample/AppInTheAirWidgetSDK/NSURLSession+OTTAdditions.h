//
//  NSURLSession+OTTAdditions.h
//  OneTwoTrip
//
//  Created by Alexander Skvortsov on 30/07/15.
//  Copyright (c) 2015 OneTwoTrip. All rights reserved.
//

@interface NSURLSession (OTTAdditions)

/*
 *  Creates url session with default settings and on serial callback queue.
 */
+ (NSURLSession *) sessionWithDelegate: (id<NSURLSessionDelegate>) delegate;

@end
